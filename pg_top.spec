Summary:	'top' for PostgreSQL process
Name:		pg_top
Version:	4.0.0
Release:    1%{?dist}
License:	BSD
Source0:	https://pg_top.gitlab.io/source/pg_top-4.0.0.tar.xz
URL:		https://pg_top.gitlab.io/
BuildRequires:	cmake
BuildRequires:	gcc
BuildRequires:	libpq-devel
BuildRequires:	readline-devel
BuildRequires:	libbsd-devel
Requires:	postgresql-server

%undefine _hardened_build
%global debug_package %{nil}

%description
pg_top is 'top' for PostgreSQL processes. See running queries, 
query plans, issued locks, and table and index statistics.

%prep
%autosetup

%build
%cmake
%cmake_build

%install
%cmake_install

%files
%{_mandir}/man1/%{name}*
%{_bindir}/%{name}
%doc HISTORY.rst README.rst TODO Y2K
%license LICENSE

%changelog
* Fri Apr 12 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 4.0.0-1
- initial build
